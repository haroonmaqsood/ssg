#!/bin/python3

import getopt, sys

sys.path.append(".")

from Dir import Dir
from Pom import Pom

def usage(exitCode):
	print("ssg -[hgavndjor]\n  -h:  help\n  -g:  group Id\n  -a:  artifact Id" +
	  "\n  -v:  artifact version  [default 0.1]\n  -n:  artifact name\n  -d:  artifact"+
	 " description  [default 'Rest Api']\n  -j:  java version  [default 11]\n  -o:  output folder\n  -r:  root"+
	 " package name")
	sys.exit(exitCode)

def main():
	gId = None
	aId = None
	ver = "0.1"
	name = None
	desc = "Rest API"
	javaVer = "11"
	oPath = None
	rootPkgNm = None

	try:
		if len(sys.argv)  == 1:
			usage(2)

		opts, args = getopt.getopt(sys.argv[1:], "hg:a:v::n:d::j::o:r:")
	except getopt.GetoptError as err:
		print(err)
		usage(2)

	for opt , arg in opts:
		if "-h" == opt:
			usage(1)
		elif "-g" == opt:
			gId = arg
		elif "-a" == opt:
			aId = arg
		elif "-v" == opt:
			ver = arg
		elif "-n" == opt:
			name = arg
		elif "-d" == opt:
			desc = arg
		elif "-j" == opt:
			javaVer = arg
		elif "-o" == opt:
			oPath = arg
		elif "-r" == opt:
			rootPkgNm = arg
		else :
			usage()
			sys.exit(2)

	try:
		dirGen  = Dir()
		dirGen.genDirs(oPath, rootPkgNm, aId)
	except Exception as e:
		print(f'Error {str(e)} genrating directories ')
		sys.exit(2)

	try:
		with open(oPath + "/application.properties", 'wt') as apFile:
			apFile.write("server.port=9090\n")
	except Exception as ex:
		print(f'Error {str(e)} genrating app props file')
		sys.exit(2)

	try:
		pom = Pom(gId, aId, ver, name, desc, javaVer)
		pom.genPom(oPath)
	except Exception as ex:
		print(f'Error {str(ex)} genrating pom.xml')
		sys.exit(2)

if __name__ == '__main__':
	main()


