
import os, re

class FileEntry:
	def __init__(self, name, contentFileName):
		self.name = name
		self.contentFileName = contentFileName

class DirEntry:
	def __init__(self, name, subDirs = None, files = None, relativeTo = ""):
		self.relativeTo = relativeTo
		self.name = name
		self.subDirs = subDirs
		self.files = files


class Dir:

	def getSrcFile(self, srcFileName, rootPkg, curPkg, aId):
		outLines = []
		rtPkgNm = rootPkg +"."
		dtoPkgNm = rtPkgNm + "dto."
		excptnPkgNm = rtPkgNm + "exception."
		curPkgNm = rtPkgNm + curPkg

		with open(self.javaFilesDir + srcFileName, "rt") as srcFile:
			for line in srcFile:
				if curPkgNm.endswith("."):
					curPkgNm = curPkgNm[:curPkgNm.rfind(".")]

				line =  line.replace("THIS_PKG", curPkgNm)
				line = line.replace("DTO_PKG", dtoPkgNm)
				line = line.replace("EXCEPTN_PKG", excptnPkgNm)
				line = line.replace("TITLE", aId+"Application".title())
				outLines.append(line)

		return outLines


	def copyUpdate(self, pkgPath, srcFileName, rootPkgName, curPkgName,
		aId):
		srcLines = self.getSrcFile(srcFileName, rootPkgName,
			curPkgName, aId)
		if srcFileName == "Main.java" :
			srcFileName = aId+"Application.java"

		with open(pkgPath + "/" + srcFileName, "wt") as destFile:
			for line in srcLines:
				destFile.write(line)

	def processDirEntry (self, dirEntry, rootPkgName, aId):
		baseName = dirEntry.relativeTo  + dirEntry.name
		if not os.path.exists(baseName):
			os.mkdir(baseName)

		if dirEntry.files is not None:
			for f in dirEntry.files:
				self.copyUpdate(baseName, f, rootPkgName,
					dirEntry.name, aId)
		if dirEntry.subDirs is None:
			return
		for entry in dirEntry.subDirs:
			entry.relativeTo = baseName + "/"
			self.processDirEntry(entry, rootPkgName, aId)



	def genDirs(self, root, rootPkgName, aId):

		aId = re.sub('[`¬!"£$%^&*()\+\=\[\]\{\};@#~''<>?/|\\-]',
		"", aId)
		aId = aId.capitalize()
		absRoot = os.path.abspath(root)

		pkgs = rootPkgName.split(".")

		pkgsLen = len(pkgs)
		if pkgsLen < 3:
			raise Exception('Package name should be atleast 3 part')

		self.javaDirEntry.subDirs = [DirEntry(pkgs[0])]

		subDirsRef = self.javaDirEntry.subDirs

		for i in range(1, pkgsLen):
			subDirsRef[0].subDirs = [DirEntry(pkgs[i])]
			subDirsRef = subDirsRef[0].subDirs

		subDirsRef[0].subDirs = []
		for entry in self.pkgDirs:
			subDirsRef[0].subDirs.append(entry)

		for rootDir in self.rootDirs:
			rootDir.relativeTo = absRoot + "/";
			self.processDirEntry(rootDir, rootPkgName, aId)


	def __init__(self):
		self.javaFilesDir = "./java/"
		self.javaDirEntry = DirEntry("java")
		self.mainDirEntry = DirEntry("main", [self.javaDirEntry])
		self.srcDir = DirEntry("src", [self.mainDirEntry])
		self.targetDir = DirEntry("target")
		self.rootDirs = [self.srcDir, self.targetDir]
		self.pkgDirs = [
			DirEntry("", files = ["Main.java"]),
			DirEntry("advice", files = ["BaseExceptionHandler.java",
				"BaseResponseInterceptor.java"]),
				DirEntry("constants", files = ["Constants.java"]),
				DirEntry("utils", files = ["Utils.java"]),
				DirEntry("controller"),
				DirEntry("service", [DirEntry("impl")]),
				DirEntry("dto", files = ["BaseDTO.java",
					"BaseErrorMessageDTO.java",
					"BaseResponseDTO.java"]),
				DirEntry("exception",
					files = ["BaseException.java"]),
				DirEntry("model"),
				DirEntry("controller",
					files = ["HelloWorldController.java"])
		]
