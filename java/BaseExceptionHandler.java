package THIS_PKG;

import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.ResponseEntity.status;

import org.slf4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import DTO_PKGBaseErrorMessageDTO;
import EXCEPTN_PKGBaseException;

@RestControllerAdvice
@Order(HIGHEST_PRECEDENCE)
public class BaseExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOGGER =
		getLogger(BaseExceptionHandler.class);

	@Override
	protected ResponseEntity<Object>
		handleMethodArgumentNotValid(MethodArgumentNotValidException
			ex, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		return status(BAD_REQUEST).body((Object)
			handleApiException(ex));
	}

	@ExceptionHandler({ Throwable.class })
	protected ResponseEntity<BaseErrorMessageDTO>
		handleApiException(Exception e) {

		BaseErrorMessageDTO errorMessageDTO = new BaseErrorMessageDTO();
		errorMessageDTO.setErrorMessage(
			INTERNAL_SERVER_ERROR.getReasonPhrase());

		int status = INTERNAL_SERVER_ERROR.value();

		if (e instanceof BaseException) {
			BaseException apiException = (BaseException) e;
			errorMessageDTO.setErrorMessage(
				apiException.getMessage());
			status = apiException.getHttpStatus();
		} else {
			LOGGER.error("------- STACK TRACE START -------");
			LOGGER.error(e.getMessage());
			StackTraceElement[] trace = e.getStackTrace();
			if (null != trace) {
				for (StackTraceElement traceElem : trace)
					LOGGER.error(traceElem.toString());
			}
			LOGGER.error("------- STACK TRACE END -------");
		}
		return status(status).body(errorMessageDTO);
	}
}
