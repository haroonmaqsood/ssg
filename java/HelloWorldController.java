package THIS_PKG;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import DTO_PKGBaseResponseDTO;

@RestController
public class HelloWorldController {

    /*NOTE* this is not a good example, use your Own DTO's that are returned from services'*/
	@GetMapping("/helloworld")
	public ResponseEntity<BaseResponseDTO> getHelloWorld() {
		return ResponseEntity.ok(new BaseResponseDTO(200, "hello world", "hello world"));
	}
}
