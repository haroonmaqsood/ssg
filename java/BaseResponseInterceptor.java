package THIS_PKG;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import DTO_PKGBaseErrorMessageDTO;
import DTO_PKGBaseResponseDTO;

@RestControllerAdvice
@Order(HIGHEST_PRECEDENCE)
public class BaseResponseInterceptor implements ResponseBodyAdvice<Object> {

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		BaseResponseDTO resp = new BaseResponseDTO();
		int status = HttpStatus.OK.value();
		String message = returnType.getMethod().getName();
		if (body instanceof BaseErrorMessageDTO) {
			message = "Error";
			status = HttpStatus.BAD_REQUEST.value();
		}
		resp.setData(body);
		resp.setMessage(message);
		resp.setStatus(status);
		return resp;
	}

}
