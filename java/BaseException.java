package THIS_PKG;

public class BaseException extends RuntimeException {
	private static final long serialVersionUID = 1891942149675660211L;
	private int httpStatus;

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public BaseException(String msg, int status) {
		super(msg);
		this.httpStatus = status;
	}
}
