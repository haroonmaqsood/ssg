package THIS_PKG;

public class BaseErrorMessageDTO {
	private String fields[];
	private String errorMessage;

	public BaseErrorMessageDTO(String[] fields, String errorMessage) {
		super();
		this.fields = fields;
		this.errorMessage = errorMessage;
	}

	public String[] getFields() {
		return fields;
	}

	public BaseErrorMessageDTO() {
		super();
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
