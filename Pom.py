

from collections import namedtuple
from lxml import etree


class Pom(object):

	def processTag(self, rootTag, tag):
		genTag = etree.SubElement(rootTag, tag.name)
		if tag.val is not None:
			genTag.text = tag.val;
		if tag.children is not None:
			for child in tag.children:
				self.processTag(genTag, child)

	def genPom(self, oPath):

		xmlns = "http://maven.apache.org/POM/4.0.0"
		xsi = "http://www.w3.org/2001/XMLSchema-instance"
		schemaLocation = "http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd"
		version = "1.0"
		ns = "{xsi}"

		projectTag = etree.Element("{" + xmlns + "}project",
			attrib = {"{" + xsi + "}schemaLocation" : schemaLocation},
			nsmap = {'xsi': xsi, None: xmlns})


		for pTag in self.projectTags:
			self.processTag(projectTag, pTag)


		with open(oPath + "/pom.xml", 'wb') as pom:
			pom.write(etree.tostring(projectTag,
			xml_declaration=True, encoding="UTF-8",
			pretty_print=True))

	def __init__(self, gId, aId, ver, name, desc, javaVer):
		self.xmlVerTag = etree.Element("xml");
		self.xmlVerTag.set("version", "1.0")
		self.xmlVerTag.set("encoding", "UTF-8")

		self.Tag = namedtuple('Tag', ['name', 'val', 'children'])

		self.projectTags = [
			self.Tag('modelVersion', '4.0.0', None),
		self.Tag('parent','', [self.Tag('groupId','org.springframework.boot',
		None), self.Tag('artifactId', 'spring-boot-starter-parent', None),
		self.Tag('version', '2.4.5', None), self.Tag('relativePath', '', None)]),
		self.Tag('groupId', gId, None),
		self.Tag('artifactId', aId, None),
		self.Tag('version', ver, None),
		self.Tag('name', name, None),
		self.Tag('description', desc, None),
		self.Tag('properties', '', [self.Tag('java.version',javaVer, None)]),
		self.Tag('dependencies', '', [self.Tag('dependency', '',
		[self.Tag('groupId', 'org.springframework.boot', None),
		self.Tag('artifactId','spring-boot-starter-web', None)]),
		self.Tag('dependency', '', [self.Tag('groupId', 'org.springframework.boot', None),
		self.Tag('artifactId', 'spring-boot-starter-web-services', None)])]),
		self.Tag('build', '', [self.Tag('plugins', '', [self.Tag('plugin', '',
		[self.Tag('groupId', 'org.springframework.boot', None),
		self.Tag('artifactId','spring-boot-maven-plugin', None)])])])
		]
